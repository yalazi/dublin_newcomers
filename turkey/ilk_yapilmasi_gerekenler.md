0. Cep telefonu hatti edinmek
1. Kalici adres edinmek
2. PPS numarasi edinmek.
3. IRP numarasi edinmek
4. Vize uzatilmasi
5. Vergi Kaydi yapilmasi


## Cep telefonu hatti edinmek
Tesco / Three / Vodafone / 48 kullanilabilir. Ayrintilar eklenecek.

## Kalici adres edinmek:
  Bu kisim bundan sonraki hayatinizin en zor noktasi olabilir. Kalici bir ev 
  bulmak icin ayri bir dosya hazirlayacagiz. Bunun icin gecici olarak 
  postalarinizi kabul edebilecek bir arkadasinizin adresini kullanabilirsiniz.
  Bu adresi, calistiginiz (is icin gelmistiniz degil mi??) is yerine ileterek,\
  "proof of address" istediginizi soylemeniz yeterli olacaktir. "Proof of Addres"
  icine "Proof of work" olmasi icin maasinizi da yazdirabilirseniz, veya ayri bir
  "Proof of Work" alirsaniz ileride hayatiniz kolaylasacaktir.
  
  Bunlar yerine, arkadasiniz gecici olarak bir faturaya adinizi ekletebilir. Ancak
  faturanin gelmesi aman alacagi icin isiniz cok uzayacaktir. Eger calismiyorsaniz
  bu yontemi kullanabilirsiniz.
  
## PPS Numarasi edinmek:
  - Detayli Bilgi icin: http://www.welfare.ie/en/Pages/Personal-Public-Service-Number-How-to-Apply.aspx
  - Randevu adresi: http://www.mywelfare.ie/
  
  Bunun icin "Proof of Address" ve Pasaportunuza ihtiyaciniz bulunuyor. Oncelikle
  wellfare sitesinden randevu almaniz gerekiyor. 2 adet randevu merkezi oldugu 
  icin 2 adresi de kontrol ederek daha yakin olan randevuyu almaniz faydaniza 
  olacaktir. PPS numarasi ve daha sonra deginecegimiz IRP numarasi Irlanda 
  hayatinizin anahtarlaridir.