# German Visa Application Via TLS Dublin

### Disclaimer: These notes are based on Oguz's experience on TLS on April 18, 2023. Feel free to add your experience by creating a PR later on. A brief explanation is that Oguz is a Turkish guy who is living in Ireland for 6+ years now, he is not Irish yet, applying with his 4-years-old kid and wife.

## Parking
There are Eurospar & Costa very close to the TLS office, feel free to park over there, there are no parking spots for TLS customers.

## Travel Insurance
You can use your existing medical insurance if you have one, I had Inspire Plus package from Laya Healthcare which covers up to 100K Euros emergency claims overseas, however, they requested to see Covid-19 coverage written. I called Laya to send me a letter, they have sent an email describing this within an hour, however, it's better to handle this a few days before your appointment.

## Videx Form - Mistakes Made

Section: Applicant's personal data
- Place of birth should be written as it is written in the Passport. If it's SISLI, write down as SISLI, don't add ISTANBUL to it.
- Name at birth: Put the before marriage surname for married people if they took their spouse's surname. (This was not a problem for my family, written down correctly)

Section: Identification papers and travel documents
- If it's issued by T.C. Dublin BE, write it down as "T.C. DUBLIN BE, IRELAND". If you are unsure, keep it as the same to what's written on your passport. (I can't say TLS employee was too worried about it, just something to mention if it's issued by some branch not in the country of that passport, you may add country information)

Section: Entry permit for the final country of destination, where applicable
- This was one of my big mistake, consider the final country of destination as where you are going to have your visit, not where you are going to return after the trip. So, I was going to go to Munich, Germany from Dublin, Ireland for a few days, in this case my final country of destination is Germany. The expectation for me is to fill information if I have some kind of permit to Germany, in my case there wasn't any, so I should have left this part empty. However, I thought "the final destination" as Dublin, Ireland as I am going to return here and put my permit information here which the TLS employee had to fix.

Section: Reference
- Choose a hotel if you are applying for a tourist visa with planning to accomodate in a hotel. The printout have this information but the form you are filling is weird that you need to choose a hotel as reference.

PS: Any mistake in the application form is going to cost you 20 euros as they suggest to fix it for you when you are there. I asked what's the alternative and they said you can book another appointment online which means you need to pay 30 euros service fee again. So, no logical alternatives if you make any mistakes.

## Applying for a Minor - Mistakes Made

### Document preparation:
- Make sure you are putting all the documents in like an adult application. Of course there are birth certificate and school reference required for them, but you also need to put flight tickets & hotel booking & proof of funds from parents and other documents as much as you can. Since I booked a group application, I was thinking flight information, health insurance, hotel booking and proof of funds would be enough if they just find in in my application, however, it wasn't. They have photocopied all those form my application and requested 0.5 euros per page.

### Videx Form
Section: Applicant's address
- Tick the box for "Residence in a country other than the country of current
nationality" if your child is Turkish but living in Ireland as in our case.

Section: Occupation
- Choose current occupation as student if your child is going to creche or school.

Section: Details regarding the right to reside in place of residence
- Make sure you leave the residence permit number empty as there is no residence permit cards for children.

Section: Travel and living costs
- Choose "a third party (host, company, organisation), please specify"
  - Then, choose "others, see section on Sponsor(s) (if different from reference)"

Section: Means of support
- Select "Assumption of all expenses during the stay"

Section: Sponsor(s) (if different from reference)
- Select "Person" as "Type of sponsor",
- Fill in the mandatory information of one parent.

That's all I have observed, please contribute further if you see any gaps here. Everyone deserves to know the process better and avoid the same mistakes.